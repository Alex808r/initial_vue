export default {
  methods: {
    // onScrollEnd() нужно переопределить в компоненте! Для напоминания используем console.warn
    onScrollEnd() {
      console.warn("You must override onScrollEnd method in component");
    },

    onScroll(event) {
      const container = event.target;
      if (
        container.clientHeight + container.scrollTop >=
        container.scrollHeight
      ) {
        this.onScrollEnd();
      }
    },
  },
};

// Object.assign({}, ...mixin, component) методы компонента имеют приоритет выше
