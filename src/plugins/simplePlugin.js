import scrollHandler from "@/mixins/scrollHandler"; // импорт миксила

export default {
  install(app) {
    app.mixin(scrollHandler); // глабальное подключение миксина

    app.config.globalProperties.$log = function () {
      console.log("Simple Plugin work");
    };
  },
};
