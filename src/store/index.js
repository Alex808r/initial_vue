import Vuex from "vuex";
import reactions from "./reactions";
import user from "./user";
// import reactions from "../mocks/reactions";

export default new Vuex.Store({
  modules: { reactions, user },

  // this.$store.state.count - доступ из компонентов( не очень хорошая практика)
  state: {
    count: 0,
  },

  // this.$store.getters.getCount - доступ из компонентов правильный способ
  // в качестве аргумента только state
  getters: {
    getCount(state) {
      return state.count;
    },
  },

  // изменения стейта только через мутации
  mutations: {
    increment(state) {
      state.count++;
    },
  },

  // являются асинхронными в них обычно вызываются мутации
  actions: {},
});
