import reactions from "../mocks/reactions";

const loadReactions = (time) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(reactions);
    }, time);
  });
};

export default {
  namespace: true,

  state: {
    reactions: [],
  },

  // в качестве аргумента только state
  getters: {
    getReactions(state) {
      return state.reactions;
    },
  },

  // изменения стейта только через мутации
  mutations: {
    SET_REACTIONS(state, payload) {
      state.reactions = payload;
    },
  },

  // payload - это то что можно передать из компонента при вызове action
  actions: {
    async loadReactions({ commit }, payload) {
      try {
        const reactions = await loadReactions(payload);
        commit("SET_REACTIONS", reactions);
        // console.error("context", context);
        // console.error(payload);
      } catch (error) {
        console.error(error);
      }
    },
  }, // отличаются от mutations тем что работаю асинхронно
};
