export default function (instance) {
  return {
    get(id) {
      return instance.get(`api/v1/films/${id}`);
    },
    getAll() {
      return instance.get("api/v1/films");
    },
  };
}
