export default function (instance) {
  return {
    signIn(payload) {
      return instance.post("users/sign_in", payload); // payload (передается при вызове из компонента )
    },
    signUp(payload) {
      return instance.post("users", payload);
    },
    logout(payload) {
      return instance.delete("users/sign_out", payload);
    },
  };
}
