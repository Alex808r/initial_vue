import { createApp } from "vue";
import App from "./App.vue";
import "@/assets/styles/main.scss";
import simplePlugin from "./plugins/simplePlugin";
import router from "./router";
import store from "./store/index";
// import { VModal } from "vue-js-modal";
import apiPlugin from "./plugins/apiPlugin";
import loadPlugin from "./plugins/loadPlugin";

// import VModal from "vue-js-modal/dist/index.nocss.js";
// import "vue-js-modal/dist/styles.css";

// import VueRouter from "vue-router";

const app = createApp(App).use(router);
// app.use(VModal);

app.use(simplePlugin);
app.use(apiPlugin);
app.use(loadPlugin);
app.use(store);

app.mount("#app");
