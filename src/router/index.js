import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import MainPage from "../pages/MainPage.vue";
import AllFilmsPage from "../pages/AllFilmsPage.vue";
import FilmPage from "../pages/FilmPage.vue";
import FilmsLayout from "../pages/FilmsLayout.vue";
import NotFound from "../pages/404.vue";
import AuthModal from "../components/step_8/AuthModal.vue";

const routes = [
  {
    path: "/",
    name: "main",
    component: MainPage,
  },
  {
    path: "/auth",
    name: "auth",
    component: AuthModal,
  },
  {
    path: "/films",
    name: "filmsLayout", //  Layout для хедеров
    component: FilmsLayout,
    children: [
      {
        path: "",
        name: "films",
        component: AllFilmsPage,
      },
      {
        path: ":id",
        name: "filmPage",
        component: FilmPage,
        // навигационный хук beforeEnter
        beforeEnter: (to, from, next) => {
          if (localStorage.getItem("user")) {
            console.log("localStorage", localStorage.getItem("user"));
            console.log("to", to);
            console.log("from", from);
            next(); // продолжит переход туда, куда переходил если без параметров (аналог push)
          } else {
            next({ name: "films" });
            console.log("localStorage", localStorage.getItem("user"));
          }
        },
      },
      {
        path: "*/*", // после id введены еще символы
        redirect: { name: "films" },
      },
    ],
  },
  {
    path: "/home",
    name: "home",
    component: HomeView,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/:catchAll(.*)",
    name: "notFound",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
